import {Manager} from './manager';
import {DEVELOPERS} from './mock-developers';

export const MANAGERS: Manager[] = [
    {
        id: 3,
        name: "Paul",
        surname: "Phill",
        age: 25,
        email: "petter.phill@gmail.com",
        group: "Conectors",
        sector: "Tech",
        teamMembers: DEVELOPERS
      },
      {
        id: 4,
        name: "Merrill",
        surname: "Mer",
        age: 30,
        email: "merrill.mer@gmail.com",
        group: "Conectors",
        sector: "Tech",
        teamMembers: DEVELOPERS
      }
  ];