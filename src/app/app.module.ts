import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProcessNumberComponent } from './process-number/process-number.component';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './/app-routing.module';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeDetComponent } from './employee-det/employee-det.component'; // <-- NgModel lives here

@NgModule({
  declarations: [
    AppComponent,
    ProcessNumberComponent,
    EmployeesComponent,
    EmployeeDetComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
