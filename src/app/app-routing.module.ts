import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProcessNumberComponent } from './process-number/process-number.component';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeDetComponent } from './employee-det/employee-det.component'

const routes: Routes = [
  { path: 'process', component: ProcessNumberComponent },
  { path: 'employees', component: EmployeesComponent },
  { path: 'employee/:id', component: EmployeeDetComponent }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}