import { Injectable } from '@angular/core';
import {Developer} from './developer';
import {Manager} from './manager';

import { DEVELOPERS } from './mock-developers';
import { MANAGERS } from './mock-managers';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor() { }
  employees: Array<Developer | Manager>;

  //In a real application this method will be retrieve data of a service, or other system, etc, but for now the data are mocked
  getEmployees(): Array<Developer | Manager> {
    if(this.employees==null){
      this.employees= DEVELOPERS;
      this.employees=this.employees.concat(MANAGERS);
    }
    return this.employees;
  }

  getEmployee(id: number): Employee {
    const employee = this.employees.filter(employee => employee.id === id)[0];
    console.log(employee);
    
    return employee;
  }

  saveEmployee(employeeToSave){

    const employeeIndex = this.employees.findIndex(
      employee => employee.id == employeeToSave.id
    );
    this.employees[employeeIndex] = employeeToSave;
  }
}

