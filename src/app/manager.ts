import {Developer} from './developer';

export class Manager extends Employee{
    group: string;
    sector: string;
    teamMembers: Array<Developer>;

    constructor(id: number, name: string, surname: string, age: number, email: string, group: string, sector: string, teamMembers: Array<Developer>){
        super(id, name, surname, age, email);
        this.group = group;
        this.sector = sector;
        this.teamMembers = teamMembers;
    }
}