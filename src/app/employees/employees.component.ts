import { Component, OnInit } from '@angular/core';
import {Developer} from '../developer';
import {Manager} from '../manager';

import { EmployeeService } from '../employee.service';


@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees: Array<Developer | Manager>;
  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees(): void {
    this.employees = this.employeeService.getEmployees();
  }


  showDevs(){
    this.employees = this.employeeService.getEmployees().filter(employee => employee.hasOwnProperty("profile"));
  }

  showManagers(){
    this.employees = this.employeeService.getEmployees().filter(employee => employee.hasOwnProperty("teamMembers"));
  }

}
