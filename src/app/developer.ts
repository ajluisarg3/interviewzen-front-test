export class Developer extends Employee{

    profile: string;
    managerId: number;
    skills: Array<string>;

    constructor(id: number, name: string, surname: string, age: number, email: string, profile: string, managerId: number, skills:Array<string>){
        super(id, name, surname, age, email);
        this.profile = profile;
        this.managerId = managerId;
        this.skills = skills;
    }
}