import {Developer} from './developer';

export const DEVELOPERS: Developer[] = [
    {
        id: 1,
        name: "Peter",
        surname: "Phill",
        age: 25,
        email: "petter.phill@gmail.com",
        profile: "Full-Stack",
        managerId: 1,
        skills: ["Angular"]
      },
      {
        id: 2,
        name: "Kenzie",
        surname: "Crisp",
        age: 30,
        email: "kenzie.crisp@gmail.com",
        profile: "Back",
        managerId: 1,
        skills: ["Spring Boot"]
      }
  ];