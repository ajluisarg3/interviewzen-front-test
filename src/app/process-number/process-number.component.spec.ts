import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessNumberComponent } from './process-number.component';

describe('ProcessNumberComponent', () => {
  let component: ProcessNumberComponent;
  let fixture: ComponentFixture<ProcessNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
