import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-process-number',
  templateUrl: './process-number.component.html',
  styleUrls: ['./process-number.component.css']
})
export class ProcessNumberComponent implements OnInit {

  outputString: string;
  
  constructor() { }

  ngOnInit() {
  }

  processString(inputString){
    const numberArray = inputString.split(/[A-Z]+/); // If the string present lower case characters the regex is /[A-Za-z]+/
    const output = numberArray.filter(c => c!=="").
        filter((val, index, array) => 
            array.indexOf(val, index +1) < 0).
                map(n => Number(n)).
                    sort((a, b) => a-b);
    
    this.outputString = output;
  }

}
