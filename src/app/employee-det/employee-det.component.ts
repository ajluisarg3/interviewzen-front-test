import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../employee.service';


@Component({
  selector: 'app-employee-det',
  templateUrl: './employee-det.component.html',
  styleUrls: ['./employee-det.component.css']
})
export class EmployeeDetComponent implements OnInit {
  employee: Employee;
  previousEmployee: Employee;
  constructor(private route: ActivatedRoute, private employeesService: EmployeeService, private location: Location) { }

  ngOnInit() {
    console.log("Init detail employee");
    this.getEmployee();
  }

  getEmployee(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    console.log("Retrieve employee with id " + id);

    this.employee = this.employeesService.getEmployee(id);
    this.previousEmployee = Object.assign({}, this.employee);
  }

  goBack() {
    this.location.back();
  }

  submitted = false;

  onCancel() {
    this.employeesService.saveEmployee(this.previousEmployee);
    this.goBack();
  }
  onSubmit(data) {
    this.submitted = true;
    this.goBack();
  }

}
